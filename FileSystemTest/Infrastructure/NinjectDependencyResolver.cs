﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using FileSystemTest.Domain.Abstract;
using FileSystemTest.Domain.Repository;

namespace FileSystemTest.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        private void AddBindings()
        {
            EFFileSystemRepository repository = new EFFileSystemRepository();

            kernel.Bind<IFileSystemRepository>().ToConstant(repository);
        }
    }
}