﻿using System.Web.Mvc;
using System.Web.Routing;

namespace FileSystemTest
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "GeneratePath",
                url: "{nodePath}",
                defaults: new { controller = "FileSystem", action = "NodesList", nodePath = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{*nodePath}",
                defaults: new { controller = "FileSystem", action = "NodesList", nodePath = UrlParameter.Optional }
            );
        }
    }
}
