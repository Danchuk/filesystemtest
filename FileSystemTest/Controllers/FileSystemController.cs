﻿using System.Collections.Generic;
using System.Web.Mvc;
using FileSystemTest.Models;
using FileSystemTest.Domain.Abstract;
using FileSystemTest.Domain.Entities;

namespace FileSystemTest.Controllers
{
    public class FileSystemController : Controller
    {
        private IFileSystemRepository repository_;

        public FileSystemController(IFileSystemRepository repo)
        {
            repository_ = repo;
        }

        public ViewResult NodesList(string nodePath)
        {
            if (nodePath == null || nodePath.Length == 0)
            {
                nodePath = FileSystemModel.PathSeparator.ToString();
            }
            else if (nodePath != FileSystemModel.PathSeparator.ToString())
            {
                nodePath = nodePath.TrimEnd(FileSystemModel.PathSeparator);
                nodePath = nodePath.TrimStart(FileSystemModel.PathSeparator);
            }
            FolderNode parentNode = repository_.GetFolderByPath(nodePath, FileSystemModel.PathSeparator.ToString());
            FileSystemModel folder = new FileSystemModel { Parent = parentNode,
                                                           Folders = repository_.GetFolderContentById(parentNode.Id),
                                                           Binaries = new List<BinaryNode>(),
                                                           ParentPath = nodePath
            };
            return View(folder);
        }
    }
}