﻿using System.Collections.Generic;
using System.Linq;
using FileSystemTest.Domain.Entities;

namespace FileSystemTest.Models
{
    public class FileSystemModel
    {
        public FolderNode Parent { get; set; }
        public IEnumerable<FolderNode> Folders { get; set; }
        public IEnumerable<BinaryNode> Binaries { get; set; }
        public string ParentPath { get; set; }

        public static char PathSeparator
        {
            get
            {
                return '/';
            }
        }

        public string CurrentNodeName
        {
            get
            {
                if (Parent.FolderNodeName == "")
                {
                    return "Root";
                }
                return Parent.FolderNodeName;
            }
        }

        public string GenerateChildPath(string child)
        {
            child = child.TrimEnd(PathSeparator);
            child = child.TrimStart(PathSeparator);

            if (ParentPath == PathSeparator.ToString())
            {
                ParentPath = Parent.FolderNodeName;
            }
            else if (ParentPath.Length > 0 && ParentPath.Last() != PathSeparator)
            {
                ParentPath = ParentPath + PathSeparator;
            }
            return ParentPath + child;
        }
    }
}