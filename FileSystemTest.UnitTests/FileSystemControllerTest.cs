﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FileSystemTest.Domain.Abstract;
using FileSystemTest.Domain.Entities;
using FileSystemTest.Controllers;
using FileSystemTest.Models;

namespace FileSystemTest.UnitTests
{
    [TestClass]
    public class FileSystemControllerTest
    {
        [TestMethod]
        public void TestRootFolder()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>
            {
                new FolderNode {FolderNodeName = "Creating Digital Images", FileSystemNodeId = 1, Id = 0 }
            });

            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "", FileSystemNodeId = 0, Id = 9 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList(null).Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "" && model.Parent.FileSystemNodeId == 0 && model.Parent.Id == 9 );
            Assert.IsTrue(model.Folders.Count() == 1);
            FolderNode child = model.Folders.ElementAt(0);
            Assert.IsTrue(child.FolderNodeName == "Creating Digital Images" && child.FileSystemNodeId == 1 && child.Id == 0);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "/");
            Assert.IsTrue(model.GenerateChildPath(child.FolderNodeName) == "Creating Digital Images");
            Assert.IsTrue(model.CurrentNodeName == "Root");
        }

        [TestMethod]
        public void TestRootFolder_AttemptWithEmptyPath()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>
            {
                new FolderNode {FolderNodeName = "Creating Digital Images", FileSystemNodeId = 1, Id = 0 }
            });

            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "", FileSystemNodeId = 0, Id = 9 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "" && model.Parent.FileSystemNodeId == 0 && model.Parent.Id == 9);
            Assert.IsTrue(model.Folders.Count() == 1);
            FolderNode child = model.Folders.ElementAt(0);
            Assert.IsTrue(child.FolderNodeName == "Creating Digital Images" && child.FileSystemNodeId == 1 && child.Id == 0);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "/");
            Assert.IsTrue(model.GenerateChildPath(child.FolderNodeName) == "Creating Digital Images");
            Assert.IsTrue(model.CurrentNodeName == "Root");
        }

        [TestMethod]
        public void TestRootFolder_AttemptWithSeparatorAsPath()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>
            {
                new FolderNode {FolderNodeName = "Creating Digital Images", FileSystemNodeId = 1, Id = 0 }
            });

            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "", FileSystemNodeId = 0, Id = 9 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("/").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "" && model.Parent.FileSystemNodeId == 0 && model.Parent.Id == 9);
            Assert.IsTrue(model.Folders.Count() == 1);
            FolderNode child = model.Folders.ElementAt(0);
            Assert.IsTrue(child.FolderNodeName == "Creating Digital Images" && child.FileSystemNodeId == 1 && child.Id == 0);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "/");
            Assert.IsTrue(model.GenerateChildPath(child.FolderNodeName) == "Creating Digital Images");
            Assert.IsTrue(model.CurrentNodeName == "Root");
        }

        [TestMethod]
        public void TestFolderContent()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>
            {
                new FolderNode { FolderNodeName = "Resources", FileSystemNodeId = 2, Id = 1},
                new FolderNode { FolderNodeName = "Evidence",  FileSystemNodeId = 3, Id = 2},
                new FolderNode { FolderNodeName = "Graphic Products", FileSystemNodeId = 4, Id = 3},
            });
            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "Creating Digital Images", FileSystemNodeId = 1, Id = 0 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("Creating Digital Images").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "Creating Digital Images" && model.Parent.FileSystemNodeId == 1 && model.Parent.Id == 0);
            Assert.IsTrue(model.Folders.Count() == 3);
            FolderNode child = model.Folders.ElementAt(0);
            Assert.IsTrue(child.FolderNodeName == "Resources" && child.FileSystemNodeId == 2 && child.Id == 1);
            child = model.Folders.ElementAt(1);
            Assert.IsTrue(child.FolderNodeName == "Evidence" && child.FileSystemNodeId == 3 && child.Id == 2);
            child = model.Folders.ElementAt(2);
            Assert.IsTrue(child.FolderNodeName == "Graphic Products" && child.FileSystemNodeId == 4 && child.Id == 3);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "Creating Digital Images");
            Assert.IsTrue(model.GenerateChildPath(child.FolderNodeName) == "Creating Digital Images/Graphic Products");
            Assert.IsTrue(model.CurrentNodeName == "Creating Digital Images");
        }

        [TestMethod]
        public void TestFolderContent_WithSeparatorInEndOfPath()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>
            {
                new FolderNode { FolderNodeName = "Resources", FileSystemNodeId = 2, Id = 1},
                new FolderNode { FolderNodeName = "Evidence",  FileSystemNodeId = 3, Id = 2},
                new FolderNode { FolderNodeName = "Graphic Products", FileSystemNodeId = 4, Id = 3},
            });
            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "Creating Digital Images", FileSystemNodeId = 1, Id = 0 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("Creating Digital Images/").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "Creating Digital Images" && model.Parent.FileSystemNodeId == 1 && model.Parent.Id == 0);
            Assert.IsTrue(model.Folders.Count() == 3);
            FolderNode child = model.Folders.ElementAt(0);
            Assert.IsTrue(child.FolderNodeName == "Resources" && child.FileSystemNodeId == 2 && child.Id == 1);
            child = model.Folders.ElementAt(1);
            Assert.IsTrue(child.FolderNodeName == "Evidence" && child.FileSystemNodeId == 3 && child.Id == 2);
            child = model.Folders.ElementAt(2);
            Assert.IsTrue(child.FolderNodeName == "Graphic Products" && child.FileSystemNodeId == 4 && child.Id == 3);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "Creating Digital Images");
            Assert.IsTrue(model.GenerateChildPath("/" + child.FolderNodeName + "/") == "Creating Digital Images/Graphic Products");
            Assert.IsTrue(model.CurrentNodeName == "Creating Digital Images");
        }

        [TestMethod]
        public void TestFolderContent_WithSeparatorInStartOfPath()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>
            {
                new FolderNode { FolderNodeName = "Resources", FileSystemNodeId = 2, Id = 1},
                new FolderNode { FolderNodeName = "Evidence",  FileSystemNodeId = 3, Id = 2},
                new FolderNode { FolderNodeName = "Graphic Products", FileSystemNodeId = 4, Id = 3},
            });
            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "Creating Digital Images", FileSystemNodeId = 1, Id = 0 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("/Creating Digital Images/").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "Creating Digital Images" && model.Parent.FileSystemNodeId == 1 && model.Parent.Id == 0);
            Assert.IsTrue(model.Folders.Count() == 3);
            FolderNode child = model.Folders.ElementAt(0);
            Assert.IsTrue(child.FolderNodeName == "Resources" && child.FileSystemNodeId == 2 && child.Id == 1);
            child = model.Folders.ElementAt(1);
            Assert.IsTrue(child.FolderNodeName == "Evidence" && child.FileSystemNodeId == 3 && child.Id == 2);
            child = model.Folders.ElementAt(2);
            Assert.IsTrue(child.FolderNodeName == "Graphic Products" && child.FileSystemNodeId == 4 && child.Id == 3);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "Creating Digital Images");
            Assert.IsTrue(model.GenerateChildPath(child.FolderNodeName) == "Creating Digital Images/Graphic Products");
            Assert.IsTrue(model.CurrentNodeName == "Creating Digital Images");
        }

        [TestMethod]
        public void TestFolderContent_WhenFolderEmpty()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>());
            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode { FolderNodeName = "Final Product", FileSystemNodeId = 8, Id = 8 });

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("/Creating Digital Images/Graphic Products/Final Product").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "Final Product" && model.Parent.FileSystemNodeId == 8 && model.Parent.Id == 8);
            Assert.IsTrue(model.Folders.Count() == 0);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "Creating Digital Images/Graphic Products/Final Product");
            Assert.IsTrue(model.CurrentNodeName == "Final Product");
        }

        [TestMethod]
        public void TestFolderContent_WhenFolderIsNotExist()
        {
            Mock<IFileSystemRepository> mock = new Mock<IFileSystemRepository>();
            mock.Setup(m => m.GetFolderContentById(It.IsAny<int>())).Returns(new List<FolderNode>());
            mock.Setup(m => m.GetFolderByPath(It.IsAny<string>(), It.IsAny<string>())).Returns(new FolderNode("53"));

            FileSystemController controller = new FileSystemController(mock.Object);

            FileSystemModel model = controller.NodesList("/Creating Digital Images/Graphic Products/Final Product/Test/53").Model as FileSystemModel;

            Assert.IsTrue(model.Parent.FolderNodeName == "53" && model.Parent.FileSystemNodeId == -1 && model.Parent.Id == -1);
            Assert.IsTrue(model.Folders.Count() == 0);
            Assert.IsTrue(model.Binaries.Count() == 0);
            Assert.IsTrue(model.ParentPath == "Creating Digital Images/Graphic Products/Final Product/Test/53");
            Assert.IsTrue(model.CurrentNodeName == "53");
        }
    }
}
