﻿using Moq;
using System;
using System.Reflection;
using System.Web;
using System.Web.Routing;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FileSystemTest.UnitTests
{
    [TestClass]
    public class RouteTests
    {
        private HttpContextBase CreateHttpContext(string targetUrl = null, string httpMethod = "GET")
        {
            Mock<HttpRequestBase> mockRequest = new Mock<HttpRequestBase>();
            mockRequest.Setup(m => m.AppRelativeCurrentExecutionFilePath)
                .Returns(targetUrl);
            mockRequest.Setup(m => m.HttpMethod).Returns(httpMethod);

            Mock<HttpResponseBase> mockResponse = new Mock<HttpResponseBase>();
            mockResponse.Setup(m => m.ApplyAppPathModifier(
                It.IsAny<string>())).Returns<string>(s => s);

            Mock<HttpContextBase> mockContext = new Mock<HttpContextBase>();
            mockContext.Setup(m => m.Request).Returns(mockRequest.Object);
            mockContext.Setup(m => m.Response).Returns(mockResponse.Object);

            return mockContext.Object;
        }

        private void TestRouteMatch(string url, string controller, string action, object routeProperties = null, string httpMethod = "GET")
        {
            RouteCollection routes = new RouteCollection();
            RouteConfig.RegisterRoutes(routes);

            RouteData result
                = routes.GetRouteData(CreateHttpContext(url, httpMethod));

            Assert.IsNotNull(result);
            Assert.IsTrue(TestIncomingRouteResult(result, controller,
                action, routeProperties));
        }

        private bool TestIncomingRouteResult(RouteData routeResult, string controller, string action, object propertySet = null)
        {
            Func<object, object, bool> valCompare = (v1, v2) =>
            {
                if(v1 == null && v2 == "")
                {
                    return true;
                }
                return StringComparer.InvariantCultureIgnoreCase.Compare(v1, v2) == 0;
            };

            bool result = valCompare(routeResult.Values["controller"], controller)
                && valCompare(routeResult.Values["action"], action);

            if (propertySet != null)
            {
                PropertyInfo[] propInfo = propertySet.GetType().GetProperties();
                foreach (PropertyInfo pi in propInfo)
                {
                    string routeVal = routeResult.Values[pi.Name] as string;
                    string propertyVal = pi.GetValue(propertySet, null) as string;
                  
                    if (!(routeResult.Values.ContainsKey(pi.Name) && valCompare(routeVal, propertyVal)))
                    {
                        result = false;
                        break;
                    }
                }
            }
            return result;
        }

        private void TestRouteFail(string url)
        {
            RouteCollection routes = new RouteCollection();
            RouteConfig.RegisterRoutes(routes);

            RouteData result = routes.GetRouteData(CreateHttpContext(url));

            Assert.IsTrue(result == null || result.Route == null);
        }

        [TestMethod]
        public void CheckRouteConfigForFileSystem()
        {
            TestRouteMatch("~/Admin/Index", "FileSystem", "NodesList", new { nodePath = "Admin/Index" });
            TestRouteMatch("~/Admin/Index/ThirdSegment", "FileSystem", "NodesList", new { nodePath = "Admin/Index/ThirdSegment" });
            TestRouteMatch("~/Admin/Index/ThirdSegment/4/5", "FileSystem", "NodesList", new { nodePath = "Admin/Index/ThirdSegment/4/5" });
            TestRouteMatch("~/Admin", "FileSystem", "NodesList", new { nodePath = "Admin" });
            TestRouteMatch("~/", "FileSystem", "NodesList", new { nodePath = "" });
        }
    }
}
