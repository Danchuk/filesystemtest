﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FileSystemTest.Domain.Entities
{
    [Table("FileSystemNode")]
    public class FileSystemNode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? ParentNodeId { get; set; }
        public string ParentNodePath { get; set; }
        public int NodeType { get; set; }
    }

    [Table("FolderNode")]
    public class FolderNode
    {
        public FolderNode()
        {
            Id = -1;
            FileSystemNodeId = -1;
            FolderNodeName = "";
        }
        public FolderNode(string currentNodeName)
        {
            Id = -1;
            FileSystemNodeId = -1;
            FolderNodeName = currentNodeName;
        }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string FolderNodeName { get; set; }
        public int FileSystemNodeId { get; set; }
    }

    [Table("BinaryNode")]
    public class BinaryNode
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string BinaryNodeName { get; set; }
        public int FileSystemNodeId { get; set; }
    }
}
