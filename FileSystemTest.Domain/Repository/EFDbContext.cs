﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using FileSystemTest.Domain.Abstract;
using FileSystemTest.Domain.Entities;

namespace FileSystemTest.Domain.Repository
{
    public class EFDbContext : DbContext
    {
        public DbSet<FolderNode> FolderNode { get; set; }
        public DbSet<BinaryNode> BinaryNode { get; set; }
        public DbSet<FileSystemNode> FileSystemNode { get; set; }

        public FolderNode GetFolderByPath(string parentNodePath, string currentNodeName)
        {
            var folders = (from fileSystemNodeTable in FileSystemNode
                           join folderNodeTable in FolderNode on fileSystemNodeTable.Id equals folderNodeTable.FileSystemNodeId
                           where fileSystemNodeTable.ParentNodePath == parentNodePath && folderNodeTable.FolderNodeName == currentNodeName
                           select new
                           {
                               Id = folderNodeTable.Id,
                               FolderNodeName = folderNodeTable.FolderNodeName,
                               FileSystemNodeId = fileSystemNodeTable.Id
                           }).OrderBy(a => a.Id).ToList();

            IEnumerable<FolderNode> retDir  = folders.Select(x => new FolderNode
            {
                Id = x.Id,
                FolderNodeName = x.FolderNodeName,
                FileSystemNodeId = x.FileSystemNodeId
            });

            return retDir.FirstOrDefault();
        }

        public IEnumerable<FolderNode> GetFolderContentById(int id)
        {
            var folders = (from fileSystemNodeTable in FileSystemNode
                           join folderNodeTable in FolderNode on fileSystemNodeTable.Id equals folderNodeTable.FileSystemNodeId
                           where fileSystemNodeTable.ParentNodeId == id && fileSystemNodeTable.NodeType == (int)NodeType.Folder
                           select new
                           {
                               Id = folderNodeTable.Id,
                               FolderNodeName = folderNodeTable.FolderNodeName,
                               FileSystemNodeId = fileSystemNodeTable.Id
                           }).OrderBy(a => a.Id).ToList();

            IEnumerable<FolderNode> retItems = folders.Select(x => new FolderNode
            {
                Id = x.Id,
                FolderNodeName = x.FolderNodeName,
                FileSystemNodeId = x.FileSystemNodeId
            });
            return retItems;
        }
    }
}
