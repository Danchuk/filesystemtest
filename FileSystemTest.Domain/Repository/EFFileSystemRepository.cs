﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemTest.Domain.Abstract;
using FileSystemTest.Domain.Entities;
using System.IO;

namespace FileSystemTest.Domain.Repository
{
    public class EFFileSystemRepository: IFileSystemRepository
    {
        private EFDbContext dbContext_ = new EFDbContext();

        public IEnumerable<FolderNode> GetFolderContentById(int id)
        {
            return dbContext_.GetFolderContentById(id);
        }

        public FolderNode GetFolderByPath(string path, string pathSeparator)
        {
            string currentNodeName = "";
            string parentPath = "";
            ParsePath(path, pathSeparator, ref currentNodeName, ref parentPath);

            FolderNode retNode = dbContext_.GetFolderByPath(parentPath, currentNodeName);

            if (retNode == null)
            {
                retNode = new FolderNode { Id = -1, FileSystemNodeId = -1, FolderNodeName = currentNodeName };
            }

            return retNode;
        }

        private void ParsePath(string path, string pathSeparator, ref string currentNodeName, ref string parentPath)
        {
            if (path == pathSeparator)
            {
                currentNodeName = "";
                parentPath = null;
            }
            else
            {
                DirectoryInfo dirInfo = new DirectoryInfo(path);
                currentNodeName = dirInfo.Name;
                parentPath = Path.GetDirectoryName(path);
                if (parentPath == "")
                {
                    parentPath = pathSeparator;
                }
                else
                {
                    parentPath = parentPath.Replace('\\', pathSeparator.First());
                    parentPath = pathSeparator + parentPath + pathSeparator;
                }
            }
        }
    }
}
