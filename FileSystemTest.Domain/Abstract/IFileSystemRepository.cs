﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FileSystemTest.Domain.Entities;

namespace FileSystemTest.Domain.Abstract
{
    enum NodeType
    {
        Folder = 1,
        Binary = 2
    }
    public interface IFileSystemRepository
    {
        IEnumerable<FolderNode> GetFolderContentById(int id);
        FolderNode GetFolderByPath(string path, string pathSeparator);
    }
}
